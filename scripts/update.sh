#!/bin/bash
set -e
helm lint ./app/ --values ./app/test.values.yaml
helm lint ./app/ --values ./app/test.values-traefikv2.yaml
helm package ./app/

helm repo index .