#!/bin/bash
set -e
helm lint ./app/ --values ./app/test.values.yaml
helm template debug ./app/ --validate --values ./app/test.values.yaml